<div class="create-user-formulaire">
	<div class="create-user-container">
		<span class="form-content">
			<span class="legend">Cr�er un utilisateur</span>
			<form method="POST" target="_parent" class="form-create-user">
		        <input class="user" type="login" name="login" placeholder="Utilisateur" required>
		        <input class="mail" type="mail" name="mail" placeholder="Adresse mail" required>
		        <input class="pwd1" type="password" name="pwd1" placeholder="Mot de passe 1" required>
		        <input class="pwd2" type="password" name="pwd2" placeholder="Mot de passe 2" required>
		        <button type="submit" class="button-submit-user">Cr�er l'utilisateur</button>
			</form>
			<span class="form-error-create-user"></span>
		</span>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){	
	$(".button-submit-user").click(function(){
		var usr_log=$(".user").val();
		var usr_mail=$(".mail").val();
		var pwd1=CryptoJS.SHA1($(".pwd1").val());
		var pwd2=CryptoJS.SHA1($(".pwd2").val());
		$.ajax({
			type: "POST",
			url: "functions/create-user.php",
			data: "login="+usr_log+"&pwd1="+pwd1+"&pwd2="+pwd2+"&mail="+usr_mail,
			success: function(html){
				if(html=='true'){
					$(".form-error-create-user").html("L'utilisateur a �t� cr��.");
				}
				else if(html=='different pwd'){
					$(".form-error-create-user").html("Les mots de passe doivent �tre identiques.");
				}
				else if(html=='Could not connect database'){
					$(".form-error-create-user").html("dberror.");
				}
				else if(html=='login already exists'){
					$(".form-error-create-user").html("Ce login existe d�j�.");
				}
				else if(html=='mail already exists'){
					$(".form-error-create-user").html("Ce mail existe d�j�.");
				}
				else if(html=='error insert'){
					$(".form-error-create-user").html("Une erreur est survenue lors de la cr�ation de l'utilisateur.");
				}
				else{
					$(".form-error-create-user").html(html);
				}
			},
			beforeSend:function(){
				$(".form-error-create-user").html("Traitement");
			}
		});
		return false;
	});
});
</script>