<?php 
session_start();
if ((isset($_SESSION['login'])&&(isset($_SESSION['pwd'])))){?>
	<form method="POST" target="_parent" class="form-change-pwd">
		<label>Mot de passe actuel</label>
		<input class="change-pwd0" type="password" name="pwd0" placeholder="Mot de passe actuel" required>
		<label>Nouveau mot de passe</label>
		<input class="change-pwd1" type="password" name="pwd1" placeholder="Nouveau mot de passe" required>
		<label>Retapez le mot de passe</label>
		<input class="change-pwd2" type="password" name="pwd2" placeholder="Nouveau mot de passe" required>
		<button type="submit" class="button-submit-change-pwd">Changer</button>	
    </form>
    <span class="form-error-pwd"></span>
<?php
}
else{
		session_unset();
		session_destroy();
		header ('location: ../index.php');
}
?>

<script type="text/javascript">
$(document).ready(function(){	
	$(".button-submit-change-pwd").click(function(){
		var pwd0=CryptoJS.SHA1($(".change-pwd0").val());
		var pwd1=CryptoJS.SHA1($(".change-pwd1").val());
		var pwd2=CryptoJS.SHA1($(".change-pwd2").val());
		if (pwd1!=pwd2){
			$(".form-error-pwd").html("Les deux nouveaux mots de passe doivent &eacirc;tre identique.");
		}
		else{
			$.ajax({
				type: "POST",
				url: "functions/change-pwd.php",
				data: "pwd0="+pwd0+"pwd1="+pwd1,
				success: function(html){
					if(html=='true'){
						$(".form-error-pwd").html("Le mot de passe a &eacute;t&eacute; chang&eacute;. Un mail vous a &eacute;t&eacute; envoy&eacute;.");
					}
					else if(html=="pwd"){
						$(".form-error-pwd").html("Le mot de passe actuel ne correspond pas.");
					}
					else{
						$(".form-error-login").html("Une erreur est survenue.<br/>Merci de contacter l'administrateur.");
					}
				}
			});
		}
	});
});
</script>