<?php 
session_start();
if (isset($_SESSION['login'])&&isset($_SESSION['pwd'])&&isset($_SESSION['rank'])&&$_SESSION['rank']==1){
	require_once '../../functions/param/main-param.php';
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		die("Could not connect database");
	}
	else{
		$sql = "SELECT * FROM user";
		$result = $conn->query($sql);
		if ($result->num_rows >= 1){
			echo "<table class='user-list'>";
			echo "<caption>Liste des utilisateurs enregistr�s</caption>";
			echo "<tr><th>Utilisateur</th><th>Email</th><th>G�rer</th></tr>";
			while($row = $result->fetch_assoc()) {
				echo "<tr>";
				echo "<td>".$row["login"]."</td><td>".$row["email"]."</td><td>"."<button class='manage-button' type='button' value='".$row["id"]."'>G�rer</button>"."</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		else {
			echo "0 results";
		}
		$conn->close();
	}
}
else{
		session_unset();
		session_destroy();
		header ('location: ../../index.php');
}
echo '<script type="text/javascript">
$(document).ready(function(){	
	$(".manage-button").click(function(){
		$(location).attr("href", "user-detail.php?id="+this.value);
	});
	return false;
});
</script>';
?>