<?php require_once '../functions/param/site-properties.php'?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <title><?php echo "$gb_siteName";?></title>
    </head>
    <body>
    	<script src="../js/jquery-1.12.0.js"></script>
    	<script src="../js/cryptoJS/rollups/sha1.js"></script>
<?php 
	session_start();
	if (isset($_SESSION['login'])&&isset($_SESSION['pwd'])&&isset($_SESSION['rank'])&&$_SESSION['rank']==1){?>
	
	
	
	<?php require_once 'header.php';?>
	
	<?php 
		require_once '../functions/param/main-param.php';
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		
		// Check connection
		if ($conn->connect_error) {
			die("Could not connect database");
		}
		else{
			if(isset($_GET["id"])){
				$userId=$_GET["id"];
				if($userId>=0){
					$sql = "SELECT * FROM user WHERE id=$userId";
					$result = $conn->query($sql);
					if ($result->num_rows >= 1){
						while($row = $result->fetch_assoc()) {
							echo "Gestion de l'utilisateur ".$row["login"];
							echo "</br>";
							echo "Cet utilisateur appartient au groupe : ";
							if($row["rank"]==1){
								echo "Administateur ";
								echo "<a href ='?id=$userId&p=0' class='btn-promote default'>D�finir comme utilisateur par d�faut</a>";
							}
							if($row["rank"]==0){
								echo "Defaut ";
								echo "<a href ='?id=$userId&p=1' class='btn-promote admin'>D�finir comme administrateur</a>";
							}
							echo "</br>";
							echo "<button class='btn-del user'>Supprimer cet utilisateur</button>";
							echo "<script type='text/javascript'>
									$(document).ready(function(){	
										$('.btn-del.user').click(function(){

								  if(confirm('Confirmer la suppression ? \\nCette action ne pourra pas �tre annul�e.')){
								   document.location.href = '?id=$userId&del=1';
								  }
									
										});
									});
								</script>";
						}
					}
					if(isset($_GET["p"])){
						$userNewRank=$_GET["p"];
						if($userNewRank==0){
							$sqlb="SELECT COUNT(rank) FROM user WHERE rank=1";
							$resultb = $conn->query($sqlb);
							if ($resultb->num_rows >= 1){
								while($rowb = $resultb->fetch_assoc()) {
									if($rowb["COUNT(rank)"]<=1){
										echo "Cet utilisateur est le seul administrateur, vous ne pouvez pas le changer de groupe";
									}
									else {
										$sqlc = "UPDATE user SET rank=$userNewRank WHERE id=$userId";
										if ($conn->query($sqlc) === TRUE) {
											echo "L'utilisateur a �t� mis � jour";
											header('Location: user-detail.php?id='.$userId);
										}
										else{
											echo "Une erreur est survenue";
										}
									}
								}
							}
						}
						if($userNewRank==1){
							$sqld = "UPDATE user SET rank=$userNewRank WHERE id=$userId";
							if ($conn->query($sqld) === TRUE) {
								echo "L'utilisateur a �t� mis � jour";
								header('Location: user-detail.php?id='.$userId);
							}
							else{
								echo "Une erreur est survenue";
							}
						}
					}
					if (isset($_GET["del"])){
						$sqle="SELECT rank FROM user WHERE id=$userId";
						$resulte = $conn->query($sqle);
						if ($resulte->num_rows >= 1){
							while($rowe = $resulte->fetch_assoc()) {
								if($rowe["rank"]==1){
									echo "Vous ne pouvez pas supprimer un administrateur.";
								}
								else{
									$sqlf="DELETE FROM user WHERE id=$userId";
									if ($conn->query($sqlf) === TRUE) {
										echo "L'utilisateur a �t� supprim�";
									} else {
										echo "Une erreur est survenue";
									}
								}
							}
						}
					}
					$conn->close();
				}
			}
		}
	?>
	
	<?php require_once 'footer.php';?>
	
	</body>
</html>
	
	
	<?php }
	else{
		session_unset();
		session_destroy();
		header ('location: ../index.php');
	}?>