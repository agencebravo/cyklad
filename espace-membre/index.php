<?php require_once '../functions/param/site-properties.php'?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <title><?php echo "$gb_siteName";?></title>
    </head>
    <body>
    	<script src="../js/jquery-1.12.0.js"></script>
    	<script src="../js/cryptoJS/rollups/sha1.js"></script>
<?php 
	session_start();
	if (isset($_SESSION['login'])&&isset($_SESSION['pwd'])){?>
	
	
	
	<?php require_once 'header.php';?>
	
	<?php require_once 'create-user-form.php';?>
	
	<?php require_once 'manage-user.php';?>
	
	<?php require_once 'footer.php';?>
	
	</body>
</html>
	
	
	<?php }
	else{
		session_unset();
		session_destroy();
		header ('location: ../index.php');
	}?>