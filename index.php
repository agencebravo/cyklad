<?php require_once 'functions/param/site-properties.php'?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <title><?php echo "$gb_siteName";?></title>
    </head>
    <body>
    	<script src="js/jquery-1.12.0.js"></script>
    	<script src="js/cryptoJS/rollups/sha1.js"></script>
		<?php require_once 'header.php';?>
		<?php require_once 'cyklad-presentation.php';?>
		<?php require_once 'login-form.php';?>
		<?php require_once 'footer.php';?>
	<script type="text/javascript" src="js/slick/slick.min.js"></script>
    </body>
</html>
