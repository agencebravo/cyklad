<?php 
session_start();
session_unset();
session_destroy();
?>
<div class="login-formulaire">
	<div class="login-container">
		<span class="form-content">
			<span class="legend">Connexion</span>
			<form class="login" action="functions/login.php" method="post">
		        <input class="user" type="login" name="login" placeholder="Utilisateur" required>
		        <input class="pwd" type="password" name="pwd" placeholder="Mot de passe" required>
		        <button type="submit" class="button-submit-login">Envoyer</button>
			</form>
			<span class="form-error-login"></span>
		</span>
		<script src="js/lightbox/lightbox-form.js" type="text/javascript"></script>
		<a href="#" onclick="openbox('Oubli du mot de passe', 0)"class="forgotten-pwd">Mot de passe oubli&eacute; ?</a>
	</div>
</div>

<div id="filter" class="forgotten-pwd"></div>
<div id="box" class="forgotten-pwd" style="position: relative;">
	<span id="boxtitle" class="forgotten-pwd"></span>
	<form method="POST" target="_parent" class="form-forgotten-pwd">
		<label>Adresse mail</label>
		<input class="user_mail" type="email" name="mail" placeholder="Adresse mail" required>
		<button type="submit" class="button-submit-pwd">Envoyer</button>
		<button type="button" class="close-pwd-lightbox" onclick="closebox()">Fermer</button>		
    </form>
    <span class="form-error-pwd"></span>
</div>

<script type="text/javascript">
$(document).ready(function(){	
	$(".button-submit-login").click(function(){
		var usr_log=$(".user").val();
		var usr_pwd=CryptoJS.SHA1($(".pwd").val());
		$.ajax({
			type: "POST",
			url: "functions/login.php",
			data: "login="+usr_log+"&pwd="+usr_pwd,
			success: function(html){
				if(html=='true'){
					//REDIRECT
					window.location.replace("espace-membre/index.php");
				}
				else if(html=='false'){
					$(".form-error-login").html("La combinaison Utilisateur et Mot de passe est incorrecte.");
				}
				else{
					$(".form-error-login").html("Erreur");
				}
			},
			beforeSend:function(){
				$(".form-error-login").html("Chargement");
			}
		});
		return false;
	});
	$(".button-submit-pwd").click(function(){
		var usr_mail=$(".user_mail").val();
		$.ajax({
			type: "POST",
			url: "functions/forgotten-pwd.php",
			data: "mail="+usr_mail,
			success: function(html){
				if(html=='true'){
					$(".form-error-pwd").html("Un email a &eacute;t&eacute; envoy&eacute; &agrave; cette adresse");
				}
				else if(html=='false-mail'){
					$(".form-error-pwd").html("Une erreur est survenue lors de l'envoi du mail.<br/>Merci de contacter l'administrateur.");
				}
				else if(html=='false-new'){
					$(".form-error-pwd").html("Une erreur est survenue lors de la cr&eacute;ation du nouveau mot de passe.<br/>Merci de contacter l'administrateur.");
				}
				else if(html=='false-addr'){
					$(".form-error-pwd").html("Cette adresse n'est li&eacute;e &agrave; aucun compte.");
				}
				else{
					$(".form-error-pwd").html("Erreur");
				}
			},
			beforeSend:function(){
				$(".form-error-pwd").html("Chargement");
			}
		});
		return false;
	});
});
</script>